package com.prodyna.springdemo.repository;


import com.prodyna.springdemo.model.HelloData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HelloRepository extends JpaRepository<HelloData, Long> {

}
