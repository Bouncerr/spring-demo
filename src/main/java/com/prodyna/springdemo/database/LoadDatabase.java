package com.prodyna.springdemo.database;

import com.prodyna.springdemo.model.HelloData;
import com.prodyna.springdemo.repository.HelloRepository;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {

    @Bean
    CommandLineRunner initDatabase(HelloRepository repository) {
        return args -> {
            repository.save(new HelloData("Bilbo Baggins", "burglar"));
            repository.save(new HelloData("Frodo Baggins", "thief"));
        };
    }
}