package com.prodyna.springdemo.controller;


import com.prodyna.springdemo.model.HelloData;
import com.prodyna.springdemo.repository.HelloRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
public class HelloController {

    @Autowired
    private HelloRepository repository;


    @GetMapping("/hellos")
    List<HelloData> all(@RequestParam(required = false) String name, @RequestParam(required = false) String role) {

        List<HelloData> data = repository.findAll();
        List<HelloData> result = new ArrayList<>();

        for (HelloData hd : data) {
            if ((name == null || name.equals(hd.getName())) && (role == null || role.equals(hd.getRole()))) {
                result.add(hd);
            }

        }

        return result;
    }

    @PostMapping("/hellos")
    HelloData newHello(@RequestBody HelloData hello) {
        return repository.save(hello);
    }

    // Single item

    @GetMapping("/hellos/{id}")
    HelloData one(@PathVariable Long id) {

        return repository.findById(id)
                .orElseThrow(() -> new HelloNotFoundException(id));
    }

    @PutMapping("/hellos/{id}")
    HelloData replaceHello(@RequestBody HelloData newHello, @PathVariable Long id) {

        return repository.findById(id)
                .map(helloData -> {
                    helloData.setName(newHello.getName());
                    helloData.setRole(newHello.getRole());
                    return repository.save(helloData);
                })
                .orElseGet(() -> {
                    newHello.setId(id);
                    return repository.save(newHello);
                });
    }

    @DeleteMapping("/hellos/{id}")
    void deleteHello(@PathVariable Long id) {
        repository.deleteById(id);
    }

    class HelloNotFoundException extends RuntimeException {
        HelloNotFoundException(Long id) {
            super("Could not find hello " + id);
        }
    }

}
