package com.prodyna.springdemo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class HelloWorldParamController {

    /**
     * Simple GET on some resource with an optional and required parameter.
     * @param id
     * @return
     */
    @GetMapping("/hello-param")
    @ResponseBody
    public String getFoos(@RequestParam(required = false) String id, @RequestParam String name) {
        return "ID: " + id + ", " + name;
    }

}
