# Spring-Demo Application

Spring-Demo Application which showcases the most common parts of a Spring Application:

- Query Parameters
- Uri Parameters
- HTTP verbs (GET, POST, PUT, PATCH)

## Spring

- @GetMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.GET). 
@GetMapping is the newer annotaion. It supports consumes. @GetMapping is a method-level.
- RequestMapping can be used at class level. RequestMapping supports consumes as well.


## Lombock

- @Data is a Lombok annotation to create all the getters, setters, equals, hash, and toString methods, based on the fields
- @Entity is a JPA annotation to make this object ready for storage in a JPA-based data store.

## JPA (Java Persistence Library)

https://de.wikipedia.org/wiki/Java_Persistence_API

Renamed in 2019 to Jakarta Persistence.

The Java Persistence API (JPA) is an interface for Java applications to simplify the association and transfer
of objects to database entries.
It simplifies the projection of objects to relational databases, meaning to store runtime objects in
Java applications over multiple sessions.


## Java Beans

https://en.wikipedia.org/wiki/JavaBeans

In computing based on the Java Platform, JavaBeans are classes that encapsulate many objects into a
single object (the bean). They are serializable, have a zero-argument constructor, and allow access to properties
using getter and setter methods. The name "Bean" was given to encompass this standard, which aims to create
reusable software components for Java. 

The required conventions are as follows:
- The class must have a public default constructor (with no arguments).
This allows easy instantiation within editing and activation frameworks.
- The class properties must be accessible using get, set, is
(can be used for boolean properties instead of get), to and other methods
(so-called accessor methods and mutator methods) according to a standard naming convention.
This allows easy automated inspection and updating of bean state within frameworks, many of which include
custom editors for various types of properties. Setters can have one or more than one argument.
- The class should be serializable. (This allows applications and frameworks to reliably save,
store, and restore the bean's state in a manner independent of the VM and of the platform.)

There is a set of interfaces that one can implement to provide further functionality:

- BeanInfo: Use the BeanInfo interface to create a BeanInfo class and provide explicit information
about the methods, properties, events, and other features of your beans.
- Customizer: A customizer class provides a complete custom GUI for customizing a target Java Bean.
- DesignMode: This interface is intended to be implemented by, or delegated from,
instances of java.beans.beancontext.BeanContext, in order to propagate to its nested hierarchy of
java.beans.beancontext.BeanContextChild instances, the current "designTime" property.
- ExceptionListener: An ExceptionListener is notified of internal exceptions.
- PropertyChangeListener: A "PropertyChange" event gets fired whenever a bean changes a "bound" property.
- PropertyEditor: A PropertyEditor class provides support for GUIs that want to allow users
to edit a property value of a given type.
- VetoableChangeListener: A VetoableChange event gets fired whenever a bean changes a "constrained" property.
- Visibility: Under some circumstances a bean may be run on servers where a GUI is not available.

Java Beans Package Summary:
https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/java/beans/package-summary.html

## Enterprise JavaBeans (EJB)

Enterprise JavaBeans (EJB) is one of several Java APIs for modular construction of enterprise software.
EJB is a server-side software component that encapsulates business logic of an application.
An EJB web container provides a runtime environment for web related software components,
including computer security, Java servlet lifecycle management, transaction processing, and other web services.
The EJB specification is a subset of the Java EE specification.

See also:
https://stackoverflow.com/questions/2460048/difference-between-java-bean-and-enterprise-java-beans


## Hibernate ORM

Hibernate ORM is an object-relational mapping tool for the Java programming language.
It provides a framework for mapping an object-oriented domain model to a relational database.
Hibernate handles object-relational impedance mismatch problems by replacing direct, persistent database accesses
with high-level object handling functions.

Hibernate's primary feature is mapping from Java classes to database tables,
and mapping from Java data types to SQL data types.
Hibernate also provides data query and retrieval facilities.
It generates SQL calls and relieves the developer from the manual handling and object conversion of the result set. 

## API Management

https://en.wikipedia.org/wiki/API_management

- Gateway: A server that acts as an API front-end, receives API requests, enforces throttling and security policies,
passes requests to the back-end service and then passes the response back to the requester.
A gateway often includes a transformation engine to orchestrate and modify the requests and responses on the fly.
A gateway can also provide functionality such as collecting analytics data and providing caching.
The gateway can provide functionality to support authentication, authorization, security, audit and regulatory compliance.
- Publishing tools: a collection of tools that API providers use to define APIs,
for instance using the OpenAPI or RAML specifications, generate API documentation, manage access and
usage policies for APIs, test and debug the execution of API.
- Developer portal/API store: community site, typically branded by an API provider, that can encapsulate for
API users in a single convenient source information and functionality including documentation,
tutorials, sample code, software development kits, an interactive API console and sandbox to trial APIs.
- Reporting and analytics: functionality to monitor API usage and load (overall hits, completed transactions,
number of data objects returned, amount of compute time and other internal resources consumed,
volume of data transferred). This can include real-time monitoring of the API with alerts being raised directly or
via a higher-level network management system, for instance.
- Monetization: functionality to support charging for access to commercial APIs.


## Tyk vs Mule

https://tyk.io and https://mulesoft.com

What | Tyk | Mule | 
 --- | --- | --- |
Licencing (Gateway, Pump) | Open source | Open core (Data Weave is closed source) |
Pricing | Mostly focuses on the number of requests per day or per second | Number of CPU cores in use |
Features | Gateway, API Management Dashboard API Developer Portal | Gateway, API Management, Design, Publishing, IDE (Anypoint Studio) |

https://www.predic8.de/tyk-open-source-api-management.htm

Beim tyk Produkt sollte man zunächst den Verwendungszweck mit der Lizenz abgleichen.
Das API Gateway und Tyk Pump für Analysedaten sind bei der Lösung von tyk Open Source.
Eine kommerzielle Nutzung dieser Komponenten ist kostenlos möglich.
Das Dashboard sowie das Developer Portal sind Closed Source und nur kostenpflichtig für kommerzielle Anwendungen nutzbar.
Privatpersonen oder gemeinnützige Vereine können Dashboard und Developer Portal kostenlos nutzen.


## RMI (Java remote method invocation)

In computing, the Java Remote Method Invocation (Java RMI) is a Java API that performs remote method invocation,
the object-oriented equivalent of remote procedure calls (RPC), with support for direct transfer of serialized
Java classes and distributed garbage-collection.



### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.0.RELEASE/maven-plugin/)

